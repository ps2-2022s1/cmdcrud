package project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.CommandLineRunner;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.Optional;
import static project.Utils.*;

@SpringBootApplication
public class Application implements CommandLineRunner {
    @Autowired 
    private TitularRepo titularRepo;
    
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    @Override
    public void run(String... args) {
        boolean sair = false;
        while (!sair) {
            mostrarMenu();
            String op = inputString("## Escolha uma opção: ");
            switch(op) {
                case "1": criarNovoTitular(); break;
                case "2": lerTodosTitulares(); break;
                case "3": lerUmTitular(); break;
                case "4": atualizarTitular(); break;
                case "5": apagarTitular(); break;
                case "6": buscarTitularPeloNome(); break;
                case "0": sair = true; break;
                default: print("\n## Opção inválida!");
            }
        }
    }
   
    private void buscarTitularPeloNome() {
        print("\n## Busca pelo nome ##");
        String nome = inputString("Nome do titular: ");
        Optional<Titular> opt = titularRepo.findByNome(nome);
        if (opt.isPresent()) {
            Titular t = opt.get();
            print("Número: " + t.getNroTitular());
            print("Nome: " + t.getNome());
            print("RG: " + t.getRg());
            print("CPF: " + t.getCpf());
        }
        else {
            print("\n## Não foi encontrado um titular com este nome!");
        }
        
    }
    
    private void atualizarTitular() {
        print("\n## Atualização de um titular");
        long nro = inputLong("Número do titular a ser atualizado: ");
        Optional<Titular> opt = titularRepo.findById(nro);
        if (opt.isPresent()) {
            Titular t = opt.get();
            String nome = inputString("Novo valor para o nome: ");
            String rg = inputString("Novo valor para o RG: ");
            String cpf = inputString("Novo valor para o CPF: ");
            t.setNome(nome);
            t.setRg(rg);
            t.setCpf(cpf);
            titularRepo.save(t);
        }
        else {
            print("\n## Não foi encontrado um titular com este número!");
        }
    }
    
    private void apagarTitular() {
        print("\n## Apagar um titular");
        long nro = inputLong("Número do titular a ser apagado: ");
        Optional<Titular> opt = titularRepo.findById(nro);
        if (opt.isPresent()) {
            Titular t = opt.get();
            titularRepo.delete(t);
        }
        else {
            print("\n## Não foi encontrado um titular com este número!");
        }
    }
    
    private void lerUmTitular() {
        print("\n## Busca de titular pelo número ##");
        long nro = inputLong("Número do titular: ");
        Optional<Titular> opt = titularRepo.findById(nro);
        if (opt.isPresent()) {
            Titular t = opt.get();
            print("Nome: " + t.getNome());
            print("RG: " + t.getRg());
            print("CPF: " + t.getCpf());
        }
        else {
            print("\n## Não foi encontrado um titular com este número!");
        }
    }
    
    private void mostrarMenu() {
        print("\n## Gerenciador de titulares ##");
        print("(1) Criar novo titular");
        print("(2) Ler todos os titulares");
        print("(3) Ler um titular");
        print("(4) Atualizar um titular");
        print("(5) Apagar um titular");
        print("(6) Buscar titular pelo nome");
        print("(0) Sair");
    }
    
    private void criarNovoTitular() {
        print("\n## Criação de novo titular ##");
        long id = inputLong("Número do titular: ");
        String nome = inputString("Nome do titular: ");
        String rg = inputString("RG: ");
        String cpf = inputString("CPF: ");
        Titular t = new Titular(id, nome, rg, cpf);
        titularRepo.save(t);
    }

    private void lerTodosTitulares() {
        print("\n## LISTA DE TODOS OS TITULARES:");
        Iterable<Titular> titulares = titularRepo.findAll();
        for (Titular t: titulares) {
            print(String.format("- %s: %s; RG: %s; CPF: %s", 
              t.getNroTitular(), t.getNome(), t.getRg(), t.getCpf()));
        }
    }
    
}