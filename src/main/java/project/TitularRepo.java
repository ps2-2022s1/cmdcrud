package project;

import org.springframework.data.repository.CrudRepository;
import java.util.Optional;

public interface TitularRepo extends CrudRepository<Titular, Long> {
    Optional<Titular> findByNome(String nome);
}
